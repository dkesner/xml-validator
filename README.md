# xml-validator

## Prerequisites
- Current version of NodeJS installed.

## Getting Started

1. Clone the project:
    ```bash
    git clone https://gitlab.com/dkesner/xml-validator.git
    ```
2. Install the project dependencies:
    ```bash
    npm install
    ```
3. Run the validation script:
    ```bash
    npm test
    ```

## Helpful Schema Validation Information

- [libxmljs](https://npmjs.com/package/libxmljs)