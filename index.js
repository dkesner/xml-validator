const xml = require('libxmljs');
const fs = require('fs');
const path = require('path');

var xsd = fs.readFileSync(path.resolve(process.cwd(), './xsd/schema.xsd')).toString();
var xsdDoc = xml.parseXmlString(xsd);

var xml0 = fs.readFileSync(path.resolve(process.cwd(), './xml/foo.xml')).toString();
var xmlDoc0 = xml.parseXmlString(xml0);

var xml1 = fs.readFileSync(path.resolve(process.cwd(), './xml/bar.xml')).toString();
var xmlDoc1 = xml.parseXmlString(xml1);

var result0 = xmlDoc0.validate(xsdDoc);
console.log("result0: ", result0);

var result1 = xmlDoc1.validate(xsdDoc);
console.log("result1: ", result1);
